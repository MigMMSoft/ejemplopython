# EjemploPython

Proyecto de ejemplo WEB usando Pytho y Django

# Detalle del sistema

 * Python 3.6
 * Pip
 * Pipenv
 * bootstrap4
 * Django 2.1.2
 * Pillow 5.3.0

 * IDE PyCharmINSTALLED_APPS = [
 	...
 	'productos',
 ]

 MEDIA_URL = '/media/'

# Creación desde 0

Para ejecutar las siguientes instrucciones debe estar instalado pip y pipenv

## Instrucciones iniciales

Ejecutar los siguientes comandos en una consola de comandos y modificar los siguientes archivos:

```bash
#Crea aplicacion
pipenv install django
django-admin startproject mi_sitio .
#Crear modulo
python .\manage.py startapp productos
#prerequisitos
pip install django-bootstrap4
pipenv install Pillow
```

Editar el archivo que se encuentra en **\mi_sitio\settings.py**

```bash
INSTALLED_APPS = [
	...
	'productos',
]

MEDIA_URL = '/media/'
```

Agregar el código en los siguiente archivos en **\produtos**

```bash
#models.py
class Producto(models.Model):
    sku = models.CharField(max_length=30)
    precio = models.IntegerField(default=0)
    descripcion = models.TextField(max_length=50)
    imagen = models.ImageField(blank=True, null=True, upload_to='gallery/%Y/%m/%d')

    def __str__(self):
        return "{}-{} {} ${}".format(self.id, self.sku,self.descripcion, self.precio)

    def get_absolute_url(self):
        return reverse('producto_edit', kwargs={'pk': self.pk})
```

```bash
#admin.py
from .models import Producto

admin.site.register(Producto)
```

```bash
#view.py
from django.views.generic import ListView
from .models import Producto

class ProductoList(ListView):
    model = Producto
    context_object_name = "prodList"
    paginate_by = 4
```

```bash
#urls.py
from . import views
urlpatterns = [
    path('', views.ProductoList.as_view(), name='producto_list'),
]
```

Archivos HTML para el despliegue **templates\base.html**

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <title>Ejemplo Django</title>
  </head>
  <body>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-8">
          <h1 class="mt-2">Lista</h1>
          <hr class="mt-0 mb-4">
          {% block content %}
          {% endblock %}
        </div>
      </div>
    </div>
  </body>
</html>
```

Segmento para la paginación **templates\paginador.html**

```html
    {% if is_paginated %}
        <div class="pagination">
            <span class="page-links">
                {% if page_obj.has_previous %}
                    <a href="/{{ pname }}?page={{ page_obj.previous_page_number }}">previous</a>
                {% endif %}
                <span class="page-current">
                    Page {{ page_obj.number }} of {{ page_obj.paginator.num_pages }}.
                </span>
                {% if page_obj.has_next %}
                    <a href="/{{ pname }}?page={{ page_obj.next_page_number }}">next</a>
                {% endif %}
            </span>
        </div>
    {% endif %}
```

Página principal **templates\productos\producto_list.html**

```html
{% extends 'base.html' %}

{% block content %}
<h2 class="mt-2">Productos</h2>
</br>
<form method="post">
<div class="table-responsive">
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">SKU</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Precio</th>
                <th scope="col"></th>
            </tr>
        </thead>
        {% for p in prodList %}
            <tr>
                <th scope="row">{{p.id}}</th>
                <td>{{p.sku}}</td>
                <td>{{p.descripcion}}</td>
                <td>{{p.precio}}</td>
                <td>
                    {% if p.imagen %}
                        <img src="{{p.imagen.url}}" style="width:32px;height:32px;"/>
                    {% endif %}
                </td>
            </tr>
        {% empty %}
        <tr><td colspan="5">Sin datos para mostrar</td></tr>
        {% endfor %}
    </table>

    {% include 'paginador.html' with pname='producto' %}
</div>


</form>
{% endblock %}
```

## Instrucciones para agregar **CRUD**

Modificar la vista para agregar las instrucciones para editar, modificar y eliminar.

```bash
#view.py
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.utils import timezone

from .models import Producto

# Create your views here.
class ProductoList(ListView):
    model = Producto
    context_object_name = "prodList"
    paginate_by = 4

    def get_queryset(self):
        queryset = Producto.objects.all()
        if self.request.GET.get("sku"):
            queryset = queryset.filter(sku__icontains=self.request.GET.get("sku"))
        if self.request.GET.get("descripcion"):
            queryset = queryset.filter(descripcion__icontains=self.request.GET.get("descripcion"))

        return queryset

class ProductoView(DetailView):
    model = Producto

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class ProductoCreate(CreateView):
    model = Producto
    fields = ['sku', 'precio', 'descripcion', 'imagen']
    success_url = reverse_lazy('producto_list')

class ProductoUpdate(UpdateView):
    model = Producto
    fields = ['sku', 'precio', 'descripcion', 'imagen']
    success_url = reverse_lazy('producto_list')

class ProductoDelete(DeleteView):
    model = Producto
    success_url = reverse_lazy('producto_list')
```

Modificar direccionamiento.

```bash
#urls.py
from django.urls import path
from . import views

urlpatterns = [
    path('', views.ProductoList.as_view(), name='producto_list'),
    path('view/<int:pk>', views.ProductoView.as_view(), name='producto_view'),
    path('new', views.ProductoCreate.as_view(), name='producto_new'),
    path('view/<int:pk>', views.ProductoView.as_view(), name='producto_view'),
    path('edit/<int:pk>', views.ProductoUpdate.as_view(), name='producto_edit'),
    path('delete/<int:pk>', views.ProductoDelete.as_view(), name='producto_delete'),
]
```

Modificar vista de la lista

```html
</br>
<form method="post">
<div class="table-responsive">
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">SKU</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Precio</th>
                <th scope="col"></th>
            </tr>
        </thead>
        {% for p in prodList %}
            <tr>
                <th scope="row">
                    <a href="{% url "producto_view" p.id %}">{{p.id}}</a>
                </th>
                <td>{{p.sku}}</td>
                <td>{{p.descripcion}}</td>
                <td>{{p.precio}}</td>
                <td>
                    {% if p.imagen %}
                        <img src="{{p.imagen.url}}" style="width:32px;height:32px;"/>
                    {% endif %}
                </td>
            </tr>
        {% empty %}
        <tr><td colspan="5">Sin datos para mostrar</td></tr>
        {% endfor %}
    </table>

    {% include 'paginador.html' with pname='producto' %}
</div>


</form>
{% endblock %}
```

Agregar vista de detalle

```html
{% extends 'base.html' %}

{% block content %}
<h2>Detalle Producto {{ now|date }}</h2>

<div class="d-flex flex-column border">
  <div class="p-2">
    <div class="d-flex flex-row">
      <div class="p-2"><a href="javascript:history.back()" class="btn btn-success">Volver</a></div>
    </div>
  </div>
  <div class="p-2">
    <div class="d-flex flex-row">
      <div class="p-2"><label for="sku">Sku: </label></div>
      <div class="p-2">{{ object.sku }}</div>
    </div>
  </div>
  <div class="p-2">
    <div class="d-flex flex-row">
      <div class="p-2"><label for="precio">Precio: </label></div>
      <div class="p-2">{{ object.precio }}</div>
    </div>
  </div>
  <div class="p-2">
    <div class="d-flex flex-row">
      <div class="p-2"><label for="descripcion">Descripcion: </label></div>
      <div class="p-2">{{ object.descripcion }}</div>
    </div>
  </div>
  <div class="p-2">
    <div class="d-flex flex-row">
      <div class="p-2"><label for="imagen">Imagen: </label></div>
      <div class="p-2">
        {% if object.imagen %}
            <img src="{{object.imagen.url}}" style="width:32px;height:32px;"/>
        {% endif %}
      </div>
    </div>
    <div class="d-flex flex-row">
      <div class="p-2"><a href="{% url "producto_edit" object.id %}" class="btn btn-success">edita</a></div>
      <div class="p-2"><a href="{% url "producto_delete" object.id %}" class="btn btn-danger">elimina</a></div>
    </div>
  </div>
</div>

{% endblock %}
```

Agregar vista de edición

```html
{% extends 'base.html' %}

{% block content %}
    <h2>Producto Edit</h2>

    <br>
    <a href="javascript:history.back()">Volver</a>
    <br>

    <form method="post">{% csrf_token %}
        {{ form.as_p }}
        <input type="submit" value="Submit" />
    </form>
{% endblock %}
```

Agregar vista de eliminación

```html
{% extends 'base.html' %}

{% block content %}
    <h1>Producto Eliminar</h1>
    <form method="post">{% csrf_token %}
      <div class="form-group">
        <label for="mensaje"> Esta seguro que decea eliminar "{{ object }}" ?</label>
        <input type="submit" value="Submit" class="btn btn-danger"/>
      </div>
    </form>
{% endblock %}
```

### Instrucciones extras

Creación de usuario de administración
```bash
python .\manage.py migrate
python .\manage.py createsuperuser
```

Cada ves que se modifique o agrege un modelo se debe ejecurar las siguietes intrucciones
```bash
python .\manage.py makemigrations
python .\manage.py migrate
```

Para la ejecución de consola para prueba de código
```bash
python .\manage.py shell
```

Para ejecución de aplicación (puerto 8000)
```bash
python .\manage.py runserver
```