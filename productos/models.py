from django.db import models
from django.urls import reverse

# Create your models here.
class Producto(models.Model):
    sku = models.CharField(max_length=30)
    precio = models.IntegerField(default=0)
    descripcion = models.TextField(max_length=50)
    imagen = models.ImageField(blank=True, null=True, upload_to='gallery/%Y/%m/%d')

    def __str__(self):
        return "{}-{} {} ${}".format(self.id, self.sku,self.descripcion, self.precio)

    def get_absolute_url(self):
        return reverse('producto_edit', kwargs={'pk': self.pk})