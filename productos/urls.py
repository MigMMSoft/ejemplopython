from django.urls import path
from . import views

urlpatterns = [
    path('', views.ProductoList.as_view(), name='producto_list'),
    path('view/<int:pk>', views.ProductoView.as_view(), name='producto_view'),
    path('new', views.ProductoCreate.as_view(), name='producto_new'),
    path('view/<int:pk>', views.ProductoView.as_view(), name='producto_view'),
    path('edit/<int:pk>', views.ProductoUpdate.as_view(), name='producto_edit'),
    path('delete/<int:pk>', views.ProductoDelete.as_view(), name='producto_delete'),
]