from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.utils import timezone

from .models import Producto

# Create your views here.
class ProductoList(ListView):
    model = Producto
    context_object_name = "prodList"
    paginate_by = 4

    def get_queryset(self):
        queryset = Producto.objects.all()
        if self.request.GET.get("sku"):
            queryset = queryset.filter(sku__icontains=self.request.GET.get("sku"))
        if self.request.GET.get("descripcion"):
            queryset = queryset.filter(descripcion__icontains=self.request.GET.get("descripcion"))

        return queryset

class ProductoView(DetailView):
    model = Producto

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class ProductoCreate(CreateView):
    model = Producto
    fields = ['sku', 'precio', 'descripcion', 'imagen']
    success_url = reverse_lazy('producto_list')

class ProductoUpdate(UpdateView):
    model = Producto
    fields = ['sku', 'precio', 'descripcion', 'imagen']
    success_url = reverse_lazy('producto_list')

class ProductoDelete(DeleteView):
    model = Producto
    success_url = reverse_lazy('producto_list')